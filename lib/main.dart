import 'package:flutter/material.dart';
import 'package:uber/screen_matching/screen_match.dart';
import 'package:uber/common/common_scaffold.dart';
import "color_scheme.dart";

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
            primarySwatch: Colors.lightGreen,
            backgroundColor: Colors.white,
            canvasColor: Colors.white,
            primaryColor: Colors.black,
            accentColor: Colors.lightGreen,
            buttonColor: Colors.lightGreen,
            errorColor: Colors.pink,
            bottomNavigationBarTheme: BottomNavigationBarThemeData(
              backgroundColor: CustomColors.darkBackgroundColor,
              selectedItemColor: Colors.white,
              unselectedItemColor: Colors.grey,
            ),
            visualDensity: VisualDensity.adaptivePlatformDensity),
        home: ProtoScaffold(ProtoMatcher()));
  }
}
