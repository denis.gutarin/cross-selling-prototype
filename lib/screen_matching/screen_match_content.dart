import 'dart:core';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:uber/screen_matching/screen_match_product.dart';

class ProtoCard extends HookWidget {
  final String imageUrl1;
  final String name1;
  final String imageUrl2;
  final String name2;

//  final List<String> propVals;

  ProtoCard({this.name1, this.imageUrl1, this.name2, this.imageUrl2});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Container(
          padding: EdgeInsets.all(12.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child:
                      ProtoProduct(name: this.name1, imageUrl: this.imageUrl1)),
              Expanded(
                  child:
                      ProtoProduct(name: this.name2, imageUrl: this.imageUrl2)),
            ],
          ),
        ));
  }
}
