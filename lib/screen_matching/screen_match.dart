import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:uber/screen_matching/screen_match_content.dart';

class ProtoMatcher extends HookWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [buildHeader(), buildCards(), buildButtons()],
    );
  }

  Widget buildHeader() {
    return Column(children: [
      SizedBox(height: 40),
      Padding(
        child: Text(
          "Эти товары применяются вместе?",
          textScaleFactor: 2,
          textAlign: TextAlign.center,
        ),
        padding: EdgeInsets.all(20),
      )
    ]);
  }

  Widget buildButtons() {
    return Column(children: [
      Padding(
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FlatButton(
              child: Text(
                "НЕТ",
                textScaleFactor: 2,
              ),
              onPressed: () {},
            ),
            FlatButton(
                child: Text(
                  "ДА",
                  textScaleFactor: 2,
                ),
                onPressed: () {}),
          ],
        ),
      ),
      SizedBox(height: 40),
    ]);
  }

  Widget buildCards() {
    return Expanded(
        child: ProtoCard(
      name1: "Лист гипсокартонный Кнауф 2500х1200х9,5 мм",
      imageUrl1:
          "https://cdn.sdvor.com/images/sdvor-catalog/350x350/0/2410.jpg?service_slug=sd_front",
      name2: "Профиль направляющий Кнауф 28×27×3000мм t=0,6",
      imageUrl2:
          "https://cdn.sdvor.com/images/sdvor-catalog/350x350/0/11515.jpg?service_slug=sd_front",
    ));
  }
}
