import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class ProtoProduct extends HookWidget {
  final String name;
  final String imageUrl;

  ProtoProduct({this.name, this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(8),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  child: Image.network(
                    this.imageUrl,
                  ),
                  flex: 8),
              Expanded(
                child: Text(
                  this.name,
                  textAlign: TextAlign.center,
                ),
                flex: 5,
              )
            ]));
  }
}
