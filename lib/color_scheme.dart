import 'package:flutter/material.dart';

class CustomColors {
  static final Color darkBackgroundColor = Color(0xFF333333);
  static final Color backgroundColor = Color(0xFFFFFFFF);
  static final Color borderColor = Color(0xFFEEEEEE);
  static final Color darkAccentColor = Color(0xFFA0AF22);
  static final Color accentColor = Color(0xFFD4E157);
  static final Color reverseColor = Color(0xFFFF6C4B);
}
