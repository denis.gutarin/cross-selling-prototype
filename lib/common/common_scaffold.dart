import 'package:flutter/material.dart';
import 'package:uber/color_scheme.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class ProtoScaffold extends HookWidget {
  final Widget _bodyWidget;
  ProtoScaffold(this._bodyWidget);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColors.darkBackgroundColor,
        title: Text("UBER"),
      ),
      body: _bodyWidget,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0,
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.fitness_center), title: Text("Тренировка")),
          BottomNavigationBarItem(
              icon: Icon(Icons.insert_link), title: Text("Связки")),
          BottomNavigationBarItem(
              icon: Icon(Icons.card_travel), title: Text("Реклама")),
          BottomNavigationBarItem(
              icon: Icon(Icons.person), title: Text("Профиль")),
        ],
      ),
    );
  }
}
